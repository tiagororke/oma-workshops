
## jewellery box

corte sem tampa 8:24


https://festi.info/boxes.py/ABox?FingerJoint_angle=90.0
&FingerJoint_style=rectangular
&FingerJoint_surroundingspaces=1
&FingerJoint_bottom_lip=0
&FingerJoint_edge_width=1
&FingerJoint_extra_length=0
&FingerJoint_finger=2.5
&FingerJoint_play=0.0
&FingerJoint_space=2.5
&FingerJoint_width=1.0
&Lid_handle=none
&Lid_style=ontop
&Lid_handle_height=8.0
&Lid_height=3
&Lid_play=0.1
&x=70
&y=70
&h=25
&outside=1
&bottom_edge=F
&thickness=5
&format=svg
&tabs=0.2
&qr_code=0
&debug=0
&labels=0
&labels=1
&reference=100
&inner_corners=corner
&burn=0.10
&language=en
&render=1


## pencilcase

corte sem tampa 9:41

https://festi.info/boxes.py/ABox?FingerJoint_angle=90.0
&FingerJoint_style=rectangular
&FingerJoint_surroundingspaces=1
&FingerJoint_bottom_lip=0
&FingerJoint_edge_width=1
&FingerJoint_extra_length=0
&FingerJoint_finger=4
&FingerJoint_play=0.0
&FingerJoint_space=4
&FingerJoint_width=1.0
&Lid_handle=none
&Lid_style=overthetop
&Lid_handle_height=8.0
&Lid_height=6
&Lid_play=0.1
&x=200
&y=80
&h=30
&outside=1
&bottom_edge=F
&thickness=5
&format=svg
&tabs=0.2
&qr_code=0
&debug=0
&labels=0
&labels=1
&reference=100
&inner_corners=corner
&burn=0.10
&language=en
&render=1


## tissuebox

https://festi.info/boxes.py/ABox?FingerJoint_angle=90.0
&FingerJoint_style=rectangular
&FingerJoint_surroundingspaces=1
&FingerJoint_bottom_lip=0
&FingerJoint_edge_width=1
&FingerJoint_extra_length=0
&FingerJoint_finger=5
&FingerJoint_play=0.0
&FingerJoint_space=4
&FingerJoint_width=1.0
&Lid_handle=none
&Lid_style=none
&Lid_handle_height=8.0
&Lid_height=6
&Lid_play=0.1
&x=235
&y=120
&h=70
&outside=0
&bottom_edge=F
&thickness=5
&format=svg
&tabs=0.2
&qr_code=0
&debug=0
&labels=0
&reference=100
&inner_corners=corner
&burn=0.10
&language=en
&render=1


## planter

~10.30
~2.30

https://festi.info/boxes.py/ABox?FingerJoint_angle=90.0
&FingerJoint_style=rectangular
&FingerJoint_surroundingspaces=1
&FingerJoint_bottom_lip=0
&FingerJoint_edge_width=1
&FingerJoint_extra_length=0
&FingerJoint_finger=3
&FingerJoint_play=0.0
&FingerJoint_space=3
&FingerJoint_width=1.3
&Lid_handle=none
&Lid_style=none
&Lid_handle_height=8.0
&Lid_height=3
&Lid_play=0.1
&x=100
&y=100
&h=85
&outside=0
&bottom_edge=h
&thickness=5
&format=svg
&tabs=0.2
&qr_code=0
&debug=0
&labels=0
&labels=1
&reference=100
&inner_corners=corner
&burn=0.10
&language=en
&render=1


